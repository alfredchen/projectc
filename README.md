# Project C
This is the patch repository for Project C, a project to provide BMQ(Bit Map Queue) and PDS-mq(Priority and Deadline based Skiplist multiple queue) cpu scheduler in one single patch set.

The patches can apply upon mainline linux kernel tree, and categoried by kernel release.

For bug reporting and discussion, please use source code repository at
* https://gitlab.com/alfredchen/linux-prjc

Third party Testing&Report
* https://docs.google.com/spreadsheets/d/163U3H-gnVeGopMrHiJLeEY1b7XlvND2yoceKbOvQRm4/edit#gid=1819453664
* https://www.reddit.com/r/linux_gaming/comments/ix6kyv/cpu_scheduler_benchmark_6_games_bmq_pds_undead_pds/

Development blog (Just for talks&discussion)
* https://cchalpha.blogspot.com


## Summer 2024 of Open Source Promotion Plan

Project C will take part in [Summer 2024 of Open Source Promotion Plan](https://summer.iscas.ac.cn/help/en/) again this year.

With the help of the plan, Project C will support of linux LTS branches(linux-5.15.y ,linux-6.1.y and linux-6.6.y).

Consider the skill of students, the task will be seperated into three phases.

1. Kick off - 2+/-1 weeks
        * Knowledge ramp up
2. Fighting - 6+/-1 weeks
        * Work on items
        * Testing
3. Finalize - 4+/-1 weeks
        * Create MR to offical repository
        * Official release lts support
